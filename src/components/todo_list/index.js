import React from 'react'
import { connect } from 'react-redux'
import { random as generateVillain } from 'supervillains'

import Todo from '@app/components/todo'
import addTodo from '@app/redux/actions/add_todo'

const mapDispatchToProps = dispatch => ({
    addTodo: text => dispatch(addTodo(text)),
})

const style = {
    listStyle: 'none',
}

export default connect(state => state, mapDispatchToProps)(
    ({ list, addTodo }) =>
        (
            <section>
                <ul
                    style={style}>
                    {
                        list.map(todo =>
                            <Todo key={todo.id} {...todo}/>
                        )
                    }
                </ul>
                <button onClick={() => addTodo(generateVillain())}>
                    PWN some other guy
                </button>
            </section>
        )
)
