import React from 'react'
import { connect } from 'react-redux'

import TodoList from '@app/components/todo_list'

export default connect(state => state)(
    props => (
        <section>
            <h1>Superhero's Todo list</h1>
            <TodoList
                {...props}/>
        </section>
    )
)
