import React from 'react'
import { connect } from 'react-redux'

import toggleTodo from '@app/redux/actions/toggle_todo'
import removeTodo from '@app/redux/actions/remove_todo'

const mapDispatchToProps = dispatch => ({
    toggleTodo: id => dispatch(toggleTodo(id)),
    removeTodo: id => dispatch(removeTodo(id)),
})

export default connect(state => state, mapDispatchToProps)(
    ({ id, text, done, toggleTodo, removeTodo }) => (
        <li>
            <button onClick={() => removeTodo(id)}>
                Spare
            </button>
            <input
                type='checkbox'
                onChange={() => toggleTodo(id)}/>
            {
                done
                    ? `${text} is PWNed (✖╭╮✖)`
                    : `PWN ${text}`
            }
        </li>
    )
)

