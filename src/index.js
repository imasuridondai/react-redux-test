import ReactDOM from 'react-dom'
import React from 'react'
import { Provider } from 'react-redux'

import App from '@app/components/app'
import store from '@app/redux/store'

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('app')
)
