import { createStore } from 'redux'

import reducer from '@app/redux/reducer'

export default createStore(reducer)
