import { random as generateVillain } from 'supervillains'
import { default as generateId } from 'uuid/v4'

const initialState = {
    list: [0, 1, 2].map(() => ({
        id: generateId(),
        text: generateVillain(),
        done: false,
    }))
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        id: generateId(),
                        text: action.text,
                        done: false,
                    },
                ]
            }
        case 'TOGGLE_TODO':
            return {
                ...state,
                list: state.list.map(todo =>
                    (todo.id === action.id)
                        ? { ...todo, done: !todo.done }
                        : todo
                )
            }
        case 'REMOVE_TODO':
            return {
                ...state,
                list: state.list.filter(todo => todo.id !== action.id),
            }
        default:
            return state
    }
}
