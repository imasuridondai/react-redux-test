const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = {
    mode: 'development',
    output: {
        filename: 'index.js',
    },
    module: {
        rules: [
            {
                test: /.js$/,
                loaders: ['babel-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            filename: 'index.html',
        }),
    ],
    resolve: {
        alias: {
            '@app': path.resolve(__dirname, 'src/'),
        },
    },
}
